import { join, resolve } from 'path';
import { Configuration, EntryObject } from 'webpack';
import { API_LIB_PATH, API_PATH, BUILD_TOOLS_PATH, mode } from './shared';

const apiFunctions = ['hello-world'];

const entry = apiFunctions.reduce(
  (e: EntryObject, name: string): EntryObject => {
    return {
      ...e,
      [name]: {
        import: `./${join(API_PATH, name)}/src/app.ts`,
        filename: `${name}/dist/app.js`,
      },
    };
  },
  {}
);

const config: Configuration = {
  mode,
  entry,
  target: 'node',
  output: {
    libraryTarget: 'commonjs2',
    path: resolve(API_PATH),
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [join(API_LIB_PATH, 'node_modules'), BUILD_TOOLS_PATH],
  },
  externals: {
    'aws-sdk': 'aws-sdk',
    'aws-lambda': 'aws-lambda',
  },
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader' }],
  },
};

export default config;
