#!/bin/bash
set -e

CONFIG_FILE=./samconfig.toml
if [ ! -f "$CONFIG_FILE" ]; then
  echo "samconfig.toml required to use deploy script. Try running 'sam deploy ---guided' and save config options at the end"
  exit 1
fi

S3BUCKET="$1"
if [ -z "$S3BUCKET" ]; then
  echo "Missing static asset S3 bucket name. Please pass it as the first argument to the deploy script."
  exit 1
fi

sam deploy

aws s3 sync ../static/dist s3://"$S3BUCKET"

