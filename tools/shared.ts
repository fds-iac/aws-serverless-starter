import { relative, resolve } from 'path';

export type WebpackMode = 'development' | 'production' | 'none';
export const mode = (process.env.NODE_ENV as WebpackMode) || 'production';
export const cwd = resolve('..', process.cwd());
export const STATIC_PATH = relative(cwd, resolve(cwd, 'static'));
export const API_PATH = relative(cwd, resolve(cwd, 'api'));
export const API_LIB_PATH = relative(cwd, resolve(cwd, 'api/libs'));
export const BUILD_TOOLS_PATH = relative(cwd, resolve(cwd, 'tools'));
