import { resolve } from 'path';
import { Configuration } from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { STATIC_PATH, mode } from './shared';

const config: Configuration & { devServer: { [index: string]: unknown } } = {
  mode,
  entry: {
    app: resolve(STATIC_PATH, 'app.ts'),
  },
  target: 'web',
  output: {
    filename: '[name].[contenthash].js',
    path: resolve(STATIC_PATH, 'dist'),
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader' }],
  },
  devServer: {
    proxy: {
      '/api': 'http://localhost:3000',
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: false,
      template: resolve(STATIC_PATH, 'index.html'),
      appMountId: 'app',
    }),
  ],
};

export default config;
