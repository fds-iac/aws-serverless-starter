const { resolve } = require('path');

const BUILD_TOOLS_PATH = resolve(process.cwd(), 'tools');

module.exports = {
  env: {
    es2021: true,
    node: true,
  },
  rules: {
    'import/no-extraneous-dependencies': [
      'error',
      {
        packageDir: ['./api/libs/'],
      },
    ],
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: `${BUILD_TOOLS_PATH}/webpack.api-config.ts`,
      },
    },
  },
};
