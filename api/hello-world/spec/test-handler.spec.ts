import {
  APIGatewayEventRequestContext,
  APIGatewayProxyEvent,
} from 'aws-lambda';
import { lambdaHandler } from '../src/app';

let event: APIGatewayProxyEvent;
let context: APIGatewayEventRequestContext;

describe('Tests hello-world handler', () => {
  it('verifies successful response', () => {
    const result = lambdaHandler(event, context);

    expect(result).toBeDefined();
    expect(result.statusCode).toEqual(200);
    expect(result.body).toBeInstanceOf(String);

    const response: { message: string } = JSON.parse(result.body) as {
      message: string;
    };

    expect(response).toBeDefined();
    expect(response.message).toBe('hello world');
  });
});
