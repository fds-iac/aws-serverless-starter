# AWS Serverless Starter

This repository provides a good starting point for building a serverless web app utilizing [AWS SAM](https://aws.amazon.com/serverless/sam/) 
with support for Cloudfront delivered static assets, typescript, jasmine, eslint, and prettier fully configured.

## Prerequisites

The Serverless Application Model Command Line Interface (SAM CLI) is an extension of the AWS CLI that adds functionality 
for building and testing Lambda applications. It uses Docker to run your functions in an Amazon Linux environment that 
matches Lambda. It can also emulate your application's build environment and API.

To use the SAM CLI, you need the following tools.

* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community)
* SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)

This project also utilizes node and NPM extensively, so be sure to install both.

* Node.js - [Install Node.js 14](https://nodejs.org/en/), including the NPM package management tool.

## Project Structure

* `api/`    - lambda handlers and test events
* `spec/`   - configuration files and helpers for the jasmine testing suite
* `static/` - front end, static, assets to be deployed to S3 and served via cloudfront
* `tools/`  - scripts and configuration for building and deploying the app

## TODO: Complete README
