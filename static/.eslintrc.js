module.exports = {
  env: {
    es2021: true,
    node: true,
  },
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      parserOptions: {
        sourceType: 'module',
        project: './static/tsconfig.json',
      },
    },
  ],
  plugins: ['html'],
};
