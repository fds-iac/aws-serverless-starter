import axios from 'axios';

async function testAPI() {
  try {
    const resp = await axios.get('/api/development/hello-world');
    console.log(resp);
  } catch (e) {
    console.error(e);
  }
}

testAPI().then(
  () => console.log('initialized app'),
  () => console.error('failed to initialize app')
);
